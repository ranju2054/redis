﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace REDIS
{
    public class DataModel
    {
        public long id { get; set; }
        public long customer_id { get; set; }
        public string customer_name { get; set; }
        public string permanent_address { get; set; }
        public string product { get; set; }
        public string account_number { get; set; }
        public string contact_number { get; set; }
    }
}
