import React, { useEffect, useState } from 'react';
import Axios from 'axios';

export const Home = () => {
  const [data, setData] = useState([])
  const [filterText, setFilterText] = useState("")

  const init = async () => {
    if(!filterText) return;
    const res = await Axios.get(`/data/${filterText}`);
    if (res) setData(res.data);
  }

  useEffect(() => { init() }, [])

  return <div>
    <input placeholder="Filter ..." onChange={e => setFilterText(e.target.value)} />
    <hr />
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Customer Id</th>
          <th>Account</th>
          <th>Name</th>
          <th>Address</th>
          <th>Contact</th>
          <th>Product</th>
        </tr>
      </thead>
      <tbody> {data && data.map(x => <tr>
            <td>{x.id}</td>
            <td>{x.customer_id}</td>
          <td>{x.account_number}</td>
            <td>{x.customer_name}</td>
            <td>{x.permanent_address}</td>
            <td>{x.contact_number}</td>
            <td>{x.product}</td>
          </tr>
          )}
          {!data.length? <tr>
            <td colSpan="7">No data found!</td>
          </tr>: null}
      </tbody>
    </table>
  </div>
}
