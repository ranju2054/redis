﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace REDIS.Controllers
{
    [ApiController]
    [Route("data")]
    public class DataController : ControllerBase
    {
        private readonly IDistributedCache _cache;
        private readonly IWebHostEnvironment _env;
        public DataController(IWebHostEnvironment env, IDistributedCache cache)
        {
            this._env = env;
            this._cache = cache;
        }


        [HttpGet("{filterText}")]
        public async Task<IActionResult> GetData(string filterText)
        {
            var cacheKey = "customerdata";
            var cachedData = await _cache.GetStringAsync(cacheKey);

            if (!string.IsNullOrWhiteSpace(cachedData))
                return Ok(JsonConvert.DeserializeObject<List<DataModel>>(cachedData).Take(20));

            var file = Path.Combine(_env.ContentRootPath, "data.json");
            var fileContent = System.IO.File.ReadAllText(file);

            //Put data in redis
            await _cache.SetStringAsync(cacheKey, fileContent);

            var data = JsonConvert.DeserializeObject<List<DataModel>>(fileContent);
            return Ok(data.Take(20));
        }
    }
}
